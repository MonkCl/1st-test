import re
import sys
from datetime import datetime
import setuptools

print(sys.version_info)
#if not sys.version_info >= (3, 8):
#    raise RuntimeError("doesn't support Python earlier than 3.8")


HERE = sys.path[0]
PACKAGE_NAME = "package"
BUILD = datetime.now().strftime('%y%m%d%H%M')


try:
    version = "1.0"
    #version = re.findall(r'^__version__ = "([^"]+)"\n?$', (HERE + "/" + PACKAGE_NAME + "/" + "__init__.py").encode("utf8"))[0]
    #, re.M
except IndexError:
    version = ""
#    raise RuntimeError("Unable to determine version")


with open(HERE + "/" + "requirements" + "/" + "production.txt") as f:
    install_requires = f.read().split('\n')
#install_requires = [
#    req.strip().strip("\r")
#    for req in (HERE + "/" + "requirements" + "/" + "production.txt").encode("utf8").split("\n")
#]

print(install_requires)
#with open(f"{PACKAGE_NAME}/build_info", "w") as f:
#    f.write(BUILD)

f = open(PACKAGE_NAME + "/build_info", "w")
f.write(BUILD)
f.close()

setuptools.setup(
    name=PACKAGE_NAME,
    version= version + "." + BUILD,
    classifiers=[
          "Programming Language :: Python :: 3.8",
          "Framework :: AsyncIO",
    ],
    python_requires=">=3.8",
    packages=[PACKAGE_NAME],
    install_requires=install_requires,
    include_package_data=True
)
